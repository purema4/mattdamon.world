document.addEventListener("DOMContentLoaded", e => {
    createMap()
    initialiseMaterialize()
    setupEvents()
}, false);

let markers = []
let map;

function createMap() {
    map = L.map('map').setView([45.4285, -75.7144], 16);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    addMarkers()

    setupLocation()
}

function fetchLocations() {
    return fetch(`/locations.json`)
    .then(blob => blob.json())
}


async function addMarkers() {

    const locations = await fetchLocations()

    for (const location of locations) {
        const marker = L.marker([location.latitude, location.longitude]).addTo(map);
        
        marker.bindPopup(`<img src="../res/${location.picture}.jpg">`, {minWidth: 301})
        markers.push(marker)
    }
}


function setupLocation() {

    let lat = document.querySelector("#lat")
    let lon = document.querySelector("#lon")

    var watchID = navigator.geolocation.watchPosition(function(position) {
       lat.innerHTML = position.coords.latitude
       lon.innerHTML = position.coords.longitude;
      });
}

function initialiseMaterialize() {

    //media
    let elems = document.querySelectorAll('.materialboxed');
    let instances = M.Materialbox.init(elems, {});
}

function setupEvents() {
    document.querySelector("#picture_taken").addEventListener("click", e => {
        document.querySelector("#upload_input").click()
        M.toast({html: 'Sending picture!'})
    })
}

document.querySelector("#upload_input").addEventListener("change", e => sendMediaToServer(e.target.files))


//TODO
// I might use cloudinary for this feature, it seems way more easier to deal with instead of doing my own server shit..
//anyway I still have to update the locations.json file with the path to the url...

function sendMediaToServer(fileList) {
        //get position
    navigator.geolocation.getCurrentPosition(position => {

        let file = null;
        let TempOut = {}
        for (let i = 0; i < fileList.length; i++) {
          if (fileList[i].type.match(/^image\//)) {
            file = fileList[i];
            break;
          }
        }


        if(file != null){
          /*  TempOut = {
                lat: position.coords.latitude,
                lon: position.coords.longitude,
                picture: file
            }*/

            fetch("/picture", { // Your POST endpoint
                method: 'POST',
                headers: {
                 // 'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: file // This is your file object
              }).then(
                response => response.json() // if the response is a JSON object
              ).then(
                success => console.log(success) // Handle the success response object
              ).catch(
                error => console.log(error) // Handle the error response object
              );
        }
        
    })    
}