const express = require("express")
const bodyParser = require("body-parser")
const helmet = require("helmet")

const path = require("path")
const fs = require("fs")
//const http2 = require("http2")
const spdy = require("spdy")

const app = express()

const SSL_PORT = process.env.SSL_PORT || 443
const PORT = process.env.PORT || 80

//http part
// Redirect from http port 80 to https
var http = require('http');
http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
}).listen(PORT);

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(helmet())

app.use(express.static(path.join(__dirname, "public")))

app.post("/picture", (req,res) => {
 //TODO
 //   fs.writeFileSync(path.join(__dirname, "./public/res", req.tempOut.picture.name), req.tempOut.picture)
})

app.get("/", (req,res) => {
    res.sendFile("index.html")
})



//keys setup
const KEY = process.env.KEY || "./keys/server.key"
const CRT = process.env.CRT || "./keys/server.crt"

spdy.createServer({
    key: fs.readFileSync(path.resolve(__dirname, KEY)),
    cert: fs.readFileSync(path.resolve(__dirname, CRT))
}, app).listen(SSL_PORT, err => {
    if (err) {
        throw new Error(err)
    }
    console.log('Listening')
})